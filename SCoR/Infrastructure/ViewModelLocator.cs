﻿using SCoR.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.Infrastructure
{
    public static class ViewModelLocator
    {
        private static MainViewModel _mainViewModel;
        public static MainViewModel MainViewModel 
        {
            get
            {
                if (_mainViewModel == null)
                    _mainViewModel = new MainViewModel();

                return _mainViewModel;
            }
        }
    }
}
