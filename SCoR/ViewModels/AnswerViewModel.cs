﻿using Prism.Commands;
using SCoR.SDK.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SCoR.ViewModels
{
    public class AnswerViewModel
    {
        #region Fields

        private readonly Answer _model;

        #endregion

        #region Properties

        public int Id => _model.Id;

        public string Title => _model.Title;

        public double Rank => _model.Rank;

        public int Score => _model.Score;

        public int UpVoteCount => _model.UpVoteCount;

        public int DownVoteCount => _model.DownVoteCount;

        public bool IsAccepted => _model.IsAccepted;

        public int CommentsUpVoteSum => _model.CommentsUpVoteSum;

        public DateTime CreationDate => _model.CreationDate;

        public string Link => _model.Link;

        public List<string> Tags => _model.Tags;

        public string Body => _model.Body;

        public string BodyAsText => _model.BodyAsText;

        public string BodyMessageOnly => _model.BodyMessageOnly;

        public string CodeSnippet
        {
            get
            {
                Debug.Assert(_model.CodeSnippets.Count < 2);

                return _model.CodeSnippets.FirstOrDefault();
            }
        }

        public int OriginalCodeLinesBefore => _model.OriginalCodeLinesBefore;

        public int CodeLinesBefore => _model.CodeLinesBefore;

        public int Cluster { get; set; }

        public int RankInCluster { get; set; }

        #endregion

        #region Commands

        public DelegateCommand OpenLinkCommand { get; }

        #endregion

        #region Constructors

        public AnswerViewModel(Answer model, int cluster, int rankInCluster)
        {
            _model = model;

            Cluster = cluster;
            RankInCluster = rankInCluster;

            OpenLinkCommand = new DelegateCommand(OpenLink);
        }

        #endregion

        #region Methods

        public void OpenLink()
        {
            try
            {
                var processInfo = new ProcessStartInfo(_model.Link)
                {
                    UseShellExecute = true
                };

                Process.Start(processInfo);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        #endregion
    }
}
