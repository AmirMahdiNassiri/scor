﻿using Prism.Mvvm;
using SCoR.SDK.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SCoR.ViewModels
{
    public class QueryResultViewModel : BindableBase
    {
        private readonly QueryResult _model;

        public List<ResultGroupViewModel> ResultGroups { get; }

        public QueryResultViewModel(QueryResult model)
        {
            _model = model;

            ResultGroups = new List<ResultGroupViewModel>();

            for (int i = 0; i < _model.ResultGroups.Count; i++)
            {
                ResultGroups.Add(new ResultGroupViewModel(i + 1, _model.ResultGroups[i]));
            }
        }
    }
}
