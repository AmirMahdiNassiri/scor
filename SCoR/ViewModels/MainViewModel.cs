﻿using Prism.Mvvm;
using Prism.Commands;
using SCoR.SDK;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace SCoR.ViewModels
{
    public class MainViewModel : BindableBase
    {
        #region Fields



        #endregion

        #region Properties

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (SetProperty(ref _isBusy, value))
                    QueryCommand?.RaiseCanExecuteChanged();
            }
        }

        private string _queryText;
        public string QueryText
        {
            get { return _queryText; }
            set
            {
                if (SetProperty(ref _queryText, value))
                    QueryCommand?.RaiseCanExecuteChanged();
            }
        }

        private string _queryCode;
        public string QueryCode
        {
            get { return _queryCode; }
            set { SetProperty(ref _queryCode, value); }
        }

        private string _queryProgrammingLanguage;
        public string QueryProgrammingLanguage
        {
            get { return _queryProgrammingLanguage; }
            set { SetProperty(ref _queryProgrammingLanguage, value); }
        }

        private string _queryTag;
        public string QueryTag
        {
            get { return _queryTag; }
            set { SetProperty(ref _queryTag, value); }
        }

        private QueryResultViewModel _queryResultViewModel;
        public QueryResultViewModel QueryResultViewModel
        {
            get { return _queryResultViewModel; }
            set { SetProperty(ref _queryResultViewModel, value); }
        }

        private object _selectedItem;
        public object SelectedItem
        {
            get { return _selectedItem; }
            set { SetProperty(ref _selectedItem, value); }
        }

        #endregion

        #region Commands

        public DelegateCommand QueryCommand { get; }

        #endregion

        #region Constructors

        public MainViewModel()
        {
            QueryCommand = new DelegateCommand(Query, CanQuery);
        }

        #endregion

        #region Command Methods

        public bool CanQuery()
        {
            return !string.IsNullOrWhiteSpace(QueryText) && !IsBusy;
        }

        public async void Query()
        {
            IsBusy = true;

            try
            {
                var queryResult = await SDK.SCoR.QueryAsync(QueryText,
                    QueryCode, QueryProgrammingLanguage, QueryTag);

                QueryResultViewModel = new QueryResultViewModel(queryResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            IsBusy = false;
        }

        #endregion
    }
}
