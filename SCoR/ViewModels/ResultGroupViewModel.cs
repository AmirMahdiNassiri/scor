﻿using Prism.Mvvm;
using SCoR.SDK.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.ViewModels
{
    public class ResultGroupViewModel : BindableBase
    {
        #region Fields

        private readonly ResultGroup _model;

        #endregion

        #region Properties

        public int Id => _model.Id;

        private int _order;
        public int Order
        {
            get { return _order; }
            set { SetProperty(ref _order, value); }
        }

        public double Rank => _model.Rank;

        public List<AnswerViewModel> Answers { get; }

        #endregion

        #region Constructors

        public ResultGroupViewModel(int order, ResultGroup model)
        {
            _model = model;

            Order = order;

            Answers = new List<AnswerViewModel>();

            for (int i = 0; i < _model.Answers.Count; i++)
            {
                Answers.Add(new AnswerViewModel(_model.Answers[i], Order, i + 1));
            }
        }

        #endregion
    }
}
