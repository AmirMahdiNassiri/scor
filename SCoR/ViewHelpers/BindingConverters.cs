﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace SCoR.ViewHelpers
{
    public class BooleanInverseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool castedValue)
                return !castedValue;
            else
                return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool castedValue)
                return !castedValue;
            else
                return Binding.DoNothing;
        }
    }

    public class ScientificNotationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var doubleValue = (double)value;

            var stringValue = doubleValue.ToString();

            if (stringValue.Length > 10)
            {
                var scientificValue = doubleValue.ToString("e");

                if (!scientificValue.EndsWith("e+000"))
                    return scientificValue;
            }

            return stringValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (int)value;

            if (castedValue == 0)
                return true;

            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public enum CompareOperation
    {
        Equal,
        LessThan,
        LessThanOrEqual,
        GreaterThan,
        GreaterThanOrEqual
    }

    public class CompareConverter : IValueConverter
    {
        public CompareOperation CompareOperation { get; set; }
        public double CompareTo { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double castedValue;

            try
            {
                castedValue = System.Convert.ToDouble(value);
            }
            catch
            {
                return Binding.DoNothing;
            }

            switch (CompareOperation)
            {
                case CompareOperation.Equal:

                    if (castedValue == CompareTo)
                        return true;
                    else
                        return false;

                case CompareOperation.LessThan:

                    if (castedValue < CompareTo)
                        return true;
                    else
                        return false;

                case CompareOperation.LessThanOrEqual:

                    if (castedValue <= CompareTo)
                        return true;
                    else
                        return false;

                case CompareOperation.GreaterThan:

                    if (castedValue > CompareTo)
                        return true;
                    else
                        return false;

                case CompareOperation.GreaterThanOrEqual:

                    if (castedValue >= CompareTo)
                        return true;
                    else
                        return false;
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public enum ArithmeticOperation
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }

    public class ArithmeticConverter : IValueConverter
    {
        public ArithmeticOperation Operation { get; set; }

        public double Operand { get; set; } = 1;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (double)value;

            return Operation switch
            {
                ArithmeticOperation.Add => castedValue + Operand,

                ArithmeticOperation.Subtract => castedValue - Operand,

                ArithmeticOperation.Multiply => castedValue * Operand,

                ArithmeticOperation.Divide => castedValue / Operand,

                _ => throw new NotImplementedException(),
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var castedValue = (double)value;

            return Operation switch
            {
                ArithmeticOperation.Add => castedValue - Operand,

                ArithmeticOperation.Subtract => castedValue + Operand,

                ArithmeticOperation.Multiply => castedValue / Operand,

                ArithmeticOperation.Divide => castedValue * Operand,

                _ => throw new NotImplementedException(),
            };
        }
    }

    public enum NullCheckMode
    {
        IsNull,
        IsNotNull
    }

    public class NullCheckToBooleanConverter : IValueConverter
    {
        public NullCheckMode Mode { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (Mode)
            {
                case NullCheckMode.IsNull:

                    if (value is null)
                        return true;

                    return false;

                case NullCheckMode.IsNotNull:

                    if (value is null)
                        return false;

                    return true;
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    /// <summary>Represents a chain of <see cref="IValueConverter"/>s to be executed in succession.</summary>
    [ContentProperty("Converters")]
    [ContentWrapper(typeof(ValueConverterCollection))]
    public class ConverterChain : IValueConverter
    {
        /// <summary>Gets the converters to execute.</summary>
        public ValueConverterCollection Converters { get; } = new ValueConverterCollection();

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Converters
                .Aggregate(value, (current, converter) => converter.Convert(current, targetType, parameter, culture));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Converters
                .Reverse()
                .Aggregate(value, (current, converter) => converter.Convert(current, targetType, parameter, culture));
        }

        #endregion
    }

    /// <summary>Represents a collection of <see cref="IValueConverter"/>s.</summary>
    public sealed class ValueConverterCollection : Collection<IValueConverter> { }
}
