# SCoR: Stack Overflow Code Recommendation Tool

Stack Overflow has become one of the main sources for developers to learn how to use different libraries, Application Programming Interfaces (APIs), and other programming concepts. Although Stack Overflow tries to rank the answers in a way that the more relevant ones are shown at top of the returned list, there are still many cases that the developers has to go through a large number of search results to find what they are really looking for, causing extra expenses in terms of both cost and time for the individuals and the companies that deal with developing software programs. Some tools such as Google Code Search, MAPO, and Strathcona have tried to re-rank the code snippets within the Stack Overflow answers, however, they have some shortages such as working only over certain projects, certain APIs, or certain programming languages, or being accessible only through a certain IDE (Integrated Development Environment). We have developed a desktop application, called SCoR, that takes the user's Stack Overflow queries online, and outputs a ranked list of code snippets, while overcoming the aforementioned drawbacks. Our evaluation shows that SCoR can meaningfully reduce the effort that the developers need to make to find useful code snippets among the large number of Stack Overflow answers.

# Run SCoR

SCoR's precompiled .Net binaries for Windows can be found under the following directory:

``SCoR\Released Packages\{version}\win-{x86,x64}\SCoR.exe``

SCoR is built using [.Net Core 3.1] and published as a [self-contained app], there is **no need to install the .Net runtime**.

# Python Requirement

SCoR's analysis module is a Python script running on Python 3.8.5. SCoR .Net application expects a [Conda] environment with the target Python version and the required dependencies installed. You can [download and install Conda from here]. Both the Anaconda or Miniconda versions can be used.

**Note that** conda executable file path should be added to system environment variables in Windows. You can verify this by running the ``conda --version`` command in CMD. This should generate an output such as: ``conda 4.8.2``. For instance, Anaconda requires adding ``C:\ProgramData\Anaconda3\condabin`` to the ``Path System Variable``. Have a look at [this Stack Overflow question] to accomplish this. **Hint:** Sometimes a system restart is required after adding a directory to the environment variables (especially when an application's code would execute the command).

The required environment of SCoR.Analysis is exported in a YAML file under in the following location:

``SCoR.Analysis\environment.yml``

You can run the following command to create a new Conda environment according to this exported YAML:

``conda env create -n SCoR -f environment.yml``

**Please note** that SCoR .Net application expects this environment to be **named "SCoR" exactly**. The .Net app uses [Conda] commands to activate the required environment and run the analysis script.

[.Net Core 3.1]: <https://dotnet.microsoft.com/download/dotnet-core/3.1>
[self-contained app]: <https://docs.microsoft.com/en-us/dotnet/core/deploying/#publish-self-contained>
[Conda]: <https://docs.conda.io/>
[download and install Conda from here]: <https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html>
[this Stack Overflow question]: <https://stackoverflow.com/questions/44597662/conda-command-is-not-recognized-on-windows-10>
