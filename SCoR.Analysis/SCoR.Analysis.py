import argparse
import json
import numpy as np
import pandas as pd
import math
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.snowball import EnglishStemmer
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import preprocessing
from sklearn.cluster import KMeans
import re
import string
from collections.abc import Iterable

np.random.seed(2020)


normalizer = preprocessing.MinMaxScaler()
stemmer = EnglishStemmer()
config = {"preprocess_text": True, "stemming": True, "stop_word_removal": True, "lower_case": True}


def get_text_vectorizer_preprocessor(s: str):

    if s is None:
        return ""

    if not config["preprocess_text"] or not config["lower_case"]:
        return s

    try:
        return s.lower()

    except BaseException:
        return s


def get_text_similarity_vectorizer(preprocess_text=config["preprocess_text"]):

    if preprocess_text:

        if config["stop_word_removal"]:

            if config["stemming"]:
                stop_words = preprocess_texts(ENGLISH_STOP_WORDS)
            else:
                stop_words = ENGLISH_STOP_WORDS

            return TfidfVectorizer(preprocessor=get_text_vectorizer_preprocessor, stop_words=stop_words)

    return TfidfVectorizer(preprocessor=get_text_vectorizer_preprocessor)


def get_word_importance_vectorizer(preprocess_text=config["preprocess_text"]):

    if preprocess_text:

        if config["stop_word_removal"]:

            stemmed_stop_words = preprocess_texts(ENGLISH_STOP_WORDS)
            return TfidfVectorizer(preprocessor=get_text_vectorizer_preprocessor, stop_words=stemmed_stop_words)

    return TfidfVectorizer(preprocessor=get_text_vectorizer_preprocessor)


def get_tag_importance_vectorizer():

    return TfidfVectorizer(preprocessor=get_text_vectorizer_preprocessor)


def preprocess_text(input_text: str):

    # input_text = re.sub(r"[{}]".format(string.punctuation), "", input_text)
    input_text = input_text.lower()
    result = re.sub(r"[().,:;?!`'\"]", ' ', input_text)
    result = ' '.join([stemmer.stem(word) for word in result.split()])

    return result


def preprocess_texts(input_texts: list):

    return [preprocess_text(text) for text in input_texts]


def preprocess_answer_tags(answer: dict):

    preprocessed_tags = list()

    for tag in answer["tags"]:

        tag = tag.replace(".", "_")
        tag = tag.replace("-", "_")
        tag = tag.replace("#", "_sharp")
        tag = tag.replace("+", "_plus")
        tag = "_" + tag

        preprocessed_tags.append(tag)

    answer["tags"] = preprocessed_tags

    return

# Applies stemming to the input array and return the stemmed array
def preprocess_answer_texts(answer: dict):

    if not config["preprocess_text"]:

        return

    if config["stemming"]:

        answer["title"] = preprocess_text(answer["title"])
        answer["body_text_only"] = preprocess_text(answer["body_text_only"])

    return


def generate_similarities(answers: list, titles_corpus: list, body_corpus: list, code_corpus: list):

    vectorizer = get_text_similarity_vectorizer()
    tfidf = vectorizer.fit_transform(titles_corpus)
    titles_similarity = tfidf * tfidf.T
    titles_similarity = titles_similarity[0, :].toarray().tolist()[0]

    vectorizer = get_text_similarity_vectorizer()
    tfidf = vectorizer.fit_transform(body_corpus)
    bodies_similarity = tfidf * tfidf.T
    bodies_similarity = bodies_similarity[0, :].toarray().tolist()[0]

    vectorizer = get_text_similarity_vectorizer(False)
    tfidf = vectorizer.fit_transform(code_corpus)
    codes_similarity = tfidf * tfidf.T
    codes_similarity = codes_similarity[0, :].toarray().tolist()[0]

    for i, a in enumerate(answers):

        a["title_similarity"] = titles_similarity[i+1]
        a["body_similarity"] = bodies_similarity[i+1]
        a["code_similarity"] = codes_similarity[i+1]

    return


def generate_word_importance(answers: list, titles_unique_corpus: list,
                             bodies_unique_corpus: list, code_corpus: list):

    vectorizer = get_word_importance_vectorizer()
    tfidf = vectorizer.fit_transform(titles_unique_corpus)
    titles_word_importance = tfidf.toarray().tolist()

    vectorizer = get_word_importance_vectorizer()
    tfidf = vectorizer.fit_transform(bodies_unique_corpus)
    bodies_word_importance = tfidf.toarray().tolist()

    vectorizer = get_word_importance_vectorizer(False)
    tfidf = vectorizer.fit_transform(code_corpus)
    codes_word_importance = tfidf.toarray().tolist()

    for i, a in enumerate(answers):

        a["title_word_importance"] = titles_word_importance[titles_unique_corpus.index(a["title"])]
        a["body_word_importance"] = bodies_word_importance[bodies_unique_corpus.index(a["body_text_only"])]
        a["code_word_importance"] = codes_word_importance[i]

    return


def generate_tag_importance(answers: list, tags_corpus: list):

    vectorizer = get_tag_importance_vectorizer()
    tfidf = vectorizer.fit_transform(tags_corpus)
    tag_importance = tfidf.toarray().tolist()

    for i, a in enumerate(answers):
        a["tag_vector"] = tag_importance[i]

    return


def read_query_result_json(file_path: str):

    questions = list()
    answers = list()
    titles_corpus = list()
    titles_unique_corpus = set()
    bodies_corpus = list()
    bodies_unique_corpus = set()
    code_corpus = list()
    tags_corpus = list()
    query_text = ""
    query_code = ""

    with open(file_path, encoding="utf8") as json_file:

        data = json.load(json_file)

        query_text = data["query_text"]
        query_code = data["query_code"]

        if config["preprocess_text"]:

            query_text = preprocess_text(query_text)

        titles_corpus.append(query_text)
        bodies_corpus.append(query_text)
        code_corpus.append(query_code)

        for i, q in enumerate(data["result_questions"]):

            questions.append(q)

            for a in q["answers"]:

                # Preprocess answer
                preprocess_answer_tags(a)

                a["code_snippets"] = "\n".join(a["code_snippets"])
                a["question_order"] = len(data["result_questions"]) - i

                if a["is_accepted"]:
                    a["is_accepted"] = 1
                else:
                    a["is_accepted"] = 0

                if config["preprocess_text"]:
                    preprocess_answer_texts(a)

                # Append answer
                answers.append(a)

                # Append corpus
                titles_corpus.append(a["title"])
                titles_unique_corpus.add(a["title"])
                bodies_corpus.append(a["body_text_only"])
                bodies_unique_corpus.add(a["body_text_only"])
                code_corpus.append(a["code_snippets"])
                tags_corpus.append(' '.join(a["tags"]))

        # Preprocess answers
        generate_similarities(answers, titles_corpus, bodies_corpus, code_corpus)
        generate_word_importance(answers, list(titles_unique_corpus), list(bodies_unique_corpus), code_corpus[1:])
        generate_tag_importance(answers, tags_corpus)

        return questions, pd.DataFrame(answers)


def normalize_data(data: pd.DataFrame, columns: list):

    for c in columns:

        # TODO: Better implementation
        data[c] = normalizer.fit_transform(np.array(data[c]).reshape(-1, 1))

    return data


def generate_ranks(data: pd.DataFrame, columns_weights_dict: dict):

    data["rank"] = np.zeros(len(data))

    for i, d in data.iterrows():

        for key in columns_weights_dict:

            data.at[i, "rank"] = data.at[i, "rank"] + d[key] * columns_weights_dict[key]


def generate_clusters(data: pd.DataFrame, columns_weights_dict: dict):

    input_data = list()

    # unique_tag_groups = set()

    vector_repetitions = list()
    max_vector_length = 0

    for i, r in data[columns_weights_dict.keys()].iterrows():

        if i == 0:

            # Find maximum length of vectors
            for c in columns_weights_dict.keys():
                if max_vector_length < len(r[c]):
                    max_vector_length = len(r[c])

            # Calculate the repetitions for each vector
            for j, c in enumerate(columns_weights_dict.keys()):

                vector_len = len(r[c])
                division = max_vector_length / vector_len
                repetition_low = math.floor(division)
                repetition_high = math.ceil(division)

                if (abs(max_vector_length - vector_len * repetition_low) <
                        abs(max_vector_length - vector_len * repetition_high)):
                    vector_repetitions.append(repetition_low)
                else:
                    vector_repetitions.append(repetition_high)

                vector_repetitions[j] = vector_repetitions[j] * columns_weights_dict[c]

        # unique_tag_groups.add("".join(str(n) for n in r["tag_vector"]))
        row = list()

        for k, c in enumerate(columns_weights_dict.keys()):

            vector = r[c]
            repeated_vector = np.tile(vector, vector_repetitions[k])

            if isinstance(vector, Iterable):
                row.extend(repeated_vector)
            else:
                row.append(repeated_vector)

        input_data.append(row)

    # cluster_count = len(unique_tag_groups)
    data["cluster"] = KMeans(n_clusters=max(int(len(input_data)/20), 1)).fit(input_data).labels_


def generate_clusters_rank(data: pd.DataFrame):

    cluster_ranks = data[["cluster", "rank"]].groupby(["cluster"]).mean().reset_index()

    data["cluster_rank"] = np.zeros(len(data))

    for i, d in data.iterrows():
        data.at[i, "cluster_rank"] = cluster_ranks[cluster_ranks["cluster"] == d["cluster"]].iloc[0]["rank"]

    return


def test():

    array = np.zeros(3)

    array[1] = 1
    array[2] = 2

    array = np.tile(array, 3)

    corpus = """
    C++
    __
    _C
    C_
    C#
    CP++
    """.split("\n")[1:-1]

    b = get_tag_importance_vectorizer()

    bag = b.fit_transform(corpus).toarray()

    columns = b.get_feature_names()

    t = get_text_similarity_vectorizer(False)

    tf_idf = t.fit_transform(corpus).toarray()

    columns = t.get_feature_names()

    return


def main(input_file: str, output_file: str):

    print("Inputting file: " + input_file)
    questions, answers_df = read_query_result_json(input_file)

    clustering_columns = {"tag_vector": 1, "title_word_importance": 1,
                          "body_word_importance": 1, "code_word_importance": 3}

    ranking_columns = {"is_accepted": 1.5, "score": 1.5, "question_order": 1,
                       "comments_up_vote_sum": 0.5, "owner_reputation": 0.5, "owner_accept_rate": 1,
                       "title_similarity": 0.5, "body_similarity": 1, "code_similarity": 2}

    normalized_ranking_data = normalize_data(answers_df, list(ranking_columns.keys()))
    generate_ranks(normalized_ranking_data, ranking_columns)
    generate_clusters(answers_df, clustering_columns)
    generate_clusters_rank(answers_df)

    with open(output_file, 'w') as file:

        for i, row in answers_df[["cluster", "cluster_rank", "answer_id", "rank", "code_snippet_index"]].iterrows():

            file.write("{0},{1},{2},{3},{4}\n".format(row["cluster"], row["cluster_rank"],
                                                      row["answer_id"], row['code_snippet_index'],
                                                      row["rank"]))

    # x = answers_df[["cluster", "cluster_rank", "rank", "body_text_only", "title",
    #                 "code_snippets", "code_similarity", "answer_id"]]\
    #     .sort_values(['cluster_rank', 'rank'], ascending=[False, False])
    # x.to_excel("output.xlsx")

    print(str(len(questions)) + " Questions")
    print(str(len(answers_df)) + " Answers")


def log_error(error, output_file: str):

    print("Error(s) occurred.")
    print(error)

    with open(output_file, 'w') as file:

        file.write(str(error))


if __name__ == '__main__':

    print("=" * 100)

    parser = argparse.ArgumentParser(description='SCoR analysis tool')
    parser.add_argument('-input', '--input', type=str, required=False, default='QueryResult.JSON',
                        help='The input query result to read the data from')
    parser.add_argument('-output', '--output', type=str, required=False, default='Output.txt',
                        help='The desired output file path')
    args = parser.parse_args()

    try:

        main(args.input, args.output)
        print("=" * 100)
        print("Program completed successfully.")

    except BaseException as e:

        log_error(e, args.output)
