﻿using System;
using SCoR.SDK;
using SCoR.SDK.Data;

namespace SCoR.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var queryText = "return char array in c";
                var queryCode = "char* char[]";
                var queryProgrammingLanguage = "c";

                var outputJsonFilePath = @"C:\dev\university\SE\SCoR\SCoR.Analysis\QueryResult.json";

                var client = new StackOverflowClient();

                var questions = client.GetQuestionsAndAnswers(queryText).Result;

                Console.WriteLine("Successfully fetched the results from the API.");
                Console.WriteLine(new string('=', 100));

                var queryResult = new QueryResult(queryText, questions, 
                    queryCode, queryProgrammingLanguage);

                queryResult.SaveToJson(outputJsonFilePath);

                Console.WriteLine("Successfully exported the answers to JSON file.");
                Console.WriteLine(new string('=', 100));

                for (int i = 0; i < questions.Count; i++)
                {
                    Console.WriteLine();
                    Console.WriteLine(new string('-', 100));
                    Console.WriteLine($"Question {i} - Title: {questions[i].Title}");
                    Console.WriteLine($"Link: {questions[i].Link}");
                    Console.WriteLine();

                    for (int j = 0; j < questions[i].Answers.Count; j++)
                    {
                        Console.WriteLine(new string('-', 50));
                        Console.WriteLine($"Answer {j} - HasCodeSnippet: {questions[i].Answers[j].HasCodeSnippet}");

                        for (int z = 0; z < questions[i].Answers[j].CodeSnippets.Count; z++)
                        {
                            Console.WriteLine(new string('-', 50));
                            Console.WriteLine($"Code snippet {z}:");
                            Console.WriteLine();
                            Console.WriteLine(questions[i].Answers[j].CodeSnippets[z]);
                            Console.WriteLine();
                        }
                    }
                }

                Console.WriteLine();
                Console.WriteLine(new string('=', 100));
                Console.WriteLine("Program finished successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(new string('=', 100));
                Console.WriteLine("Exception occurred:");
                Console.WriteLine();
                Console.WriteLine(ex);
            }

            Console.WriteLine(new string('=', 100));
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
