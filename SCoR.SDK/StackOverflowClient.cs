﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SCoR.SDK.Data;
using SCoR.SDK.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace SCoR.SDK
{
    public class StackOverflowClient
    {
        #region Const Configurations

        public const string ApiUrl = "https://api.stackexchange.com/";
        public const string ApiVersion = "2.2";
        public const string ApiRoot = ApiUrl + ApiVersion;

        #endregion

        #region Private Methods

        private HttpClient GetDefaultClient()
        {
            var result = new HttpClient
            {
                BaseAddress = new Uri(ApiRoot)
            };

            result.DefaultRequestHeaders.Accept.Clear();
            result.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            return result;
        }

        private NameValueCollection GetDefaultQuery()
        {
            var result = HttpUtility.ParseQueryString("site=stackoverflow");

            // See: https://api.stackexchange.com/docs/filters
            result["filter"] = "!Tc1vVoy_PY6zg*s1Fsd6U0yiEY__Dm5VMvt*Aw_78gV8q*-_oi9";
            result["order"] = "desc";
            result["pagesize"] = "30";

            return result;
        }

        private void RemoveNegativeScoreAnswers(List<Question> result)
        {
            foreach (var q in result)
            {
                var removedCount = q.Answers.RemoveAll(a => a.Score < 0 && !a.IsAccepted);

                q.AnswerCount -= removedCount;
            }

            result.RemoveAll(q => q.AnswerCount <= 0);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// See: https://api.stackexchange.com/docs/advanced-search
        /// </summary>
        /// <param name="query">A free form text parameter, will match all question properties based on an undocumented algorithm.</param>
        /// <param name="minimumAnswerCount">The minimum number of answers returned questions must have.</param>
        /// <param name="accepted">True to return only questions with accepted answers, False to return only those without. Omit to elide constraint.</param>
        /// <returns></returns>
        public async Task<List<Question>> GetQuestions(string query, 
            int minimumAnswerCount = 1, bool? accepted = null,
            bool loadAnswers = false)
        {
            var client = GetDefaultClient();

            var queryString = GetDefaultQuery();

            queryString["q"] = query;
            queryString["sort"] = "relevance";
            queryString["answers"] = minimumAnswerCount.ToString();

            if (accepted.HasValue)
                queryString["accepted"] = accepted.Value.ToString();

            var response = await client.GetAsync("/search/advanced?" + queryString.ToString());

            if (response.IsSuccessStatusCode)
            {
                using var s = await response.Content.ReadAsStreamAsync();
                using var decompressed = new GZipStream(s, CompressionMode.Decompress);
                using var reader = new StreamReader(decompressed);

                var contentString = await reader.ReadToEndAsync();

                var json = JObject.Parse(contentString);
                var items = json.SelectToken("items");

                var result = items.ToObject<List<Question>>();

                if (loadAnswers)
                {
                    var questionDict = new Dictionary<int, Question>();

                    foreach (var question in result)
                    {
                        questionDict.Add(question.Id, question);
                    }

                    var answers = await GetAnswers(result.Select(q => q.Id));

                    foreach (var a in answers)
                    {
                        questionDict[a.QuestionId].Answers.Add(a);
                    }
                }

                return result;
            }

            throw new StackExchangeApiException("Couldn't get success status code.", response.StatusCode);
        }

        /// <summary>
        /// See: https://api.stackexchange.com/docs/answers-on-questions
        /// </summary>
        /// <returns></returns>
        public async Task<List<Answer>> GetAnswers(IEnumerable<int> ids)
        {
            var client = GetDefaultClient();

            var idsString = string.Join(";", ids);

            var queryString = GetDefaultQuery();
            queryString["sort"] = "votes";

            var response = await client.GetAsync($"/questions/{idsString}/answers?" + queryString.ToString());

            if (response.IsSuccessStatusCode)
            {
                using var s = await response.Content.ReadAsStreamAsync();
                using var decompressed = new GZipStream(s, CompressionMode.Decompress);
                using var reader = new StreamReader(decompressed);

                var contentString = await reader.ReadToEndAsync();

                var json = JObject.Parse(contentString);
                var items = json.SelectToken("items");

                var result = items.ToObject<List<Answer>>();

                return result;
            }

            throw new StackExchangeApiException("Couldn't get success status code.", response.StatusCode);
        }

        /// <summary>
        /// See: https://api.stackexchange.com/docs/advanced-search
        /// </summary>
        /// <param name="query">A free form text parameter, will match all question properties based on an undocumented algorithm.</param>
        /// <param name="minimumAnswerCount">The minimum number of answers returned questions must have.</param>
        /// <param name="accepted">True to return only questions with accepted answers, False to return only those without. Omit to elide constraint.</param>
        /// <returns></returns>
        public async Task<List<Question>> GetQuestionsAndAnswers(string query,
             string tag = null, int minimumAnswerCount = 1, bool? accepted = null,
             bool removeNegativeScoreAnswers = true)
        {
            var client = GetDefaultClient();

            var queryString = GetDefaultQuery();

            queryString["q"] = query;
            queryString["sort"] = "relevance";
            // NOTE: How does this method sort the answers?!
            queryString["answers"] = minimumAnswerCount.ToString();

            if (!string.IsNullOrWhiteSpace(tag))
                queryString["tagged"] = tag;

            if (accepted.HasValue)
                queryString["accepted"] = accepted.Value.ToString();

            var response = await client.GetAsync("/search/advanced?" + queryString.ToString());

            if (response.IsSuccessStatusCode)
            {
                using var s = await response.Content.ReadAsStreamAsync();
                using var decompressed = new GZipStream(s, CompressionMode.Decompress);
                using var reader = new StreamReader(decompressed);

                var contentString = await reader.ReadToEndAsync();

                var json = JObject.Parse(contentString);
                var items = json.SelectToken("items");

                var result = items.ToObject<List<Question>>();

                if (removeNegativeScoreAnswers)
                    RemoveNegativeScoreAnswers(result);

                return result;
            }

            throw new StackExchangeApiException("Couldn't get success status code.", response.StatusCode);
        }

        #endregion
    }
}
