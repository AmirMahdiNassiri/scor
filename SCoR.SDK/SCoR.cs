﻿using SCoR.SDK.Data;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCoR.SDK
{
    public static class SCoR
    {
        private static StackOverflowClient _client = new StackOverflowClient();

        public static QueryResult Query(string queryText, string queryCode = null,
            string queryProgrammingLanguage = null, string queryTag = null)
        {
            List<Question> resultQuestions;

            if (string.IsNullOrWhiteSpace(queryTag))
                resultQuestions = _client.GetQuestionsAndAnswers(queryText).Result;
            else
                resultQuestions = _client.GetQuestionsAndAnswers(queryText, queryTag).Result;

            var queryResult = new QueryResult(queryText, resultQuestions,
                queryCode, queryProgrammingLanguage, queryTag);

            queryResult.SaveToJson(DirectoryManager.QueryResultJsonFile);

            RunAnalysis();

            var lines = File.ReadAllLines(DirectoryManager.OutputAnalysisResultFile);

            if (lines.Length < queryResult.AllAvailableAnswers.Count)
            {
                var exceptionMessage = lines.FirstOrDefault();

                if (string.IsNullOrWhiteSpace(exceptionMessage))
                    throw new Exception("Unknown exception occurred in analysis.");
                else
                    throw new Exception($"The following exception occurred in analysis:{Environment.NewLine}{Environment.NewLine}{exceptionMessage}");
            }

            foreach (var l in lines)
            {
                if (string.IsNullOrWhiteSpace(l))
                    continue;

                var values = l.Split(',');

                if (values.Length < 5)
                    continue;

                var clusterId = (int)double.Parse(values[0]);
                var clusterRank = double.Parse(values[1]);
                var answerId = (int)double.Parse(values[2]);
                var answerCodeSnippetIndex = (int)double.Parse(values[3]);
                var answerRank = double.Parse(values[4]);

                var answer = queryResult.AllAvailableAnswers.First(a =>
                    (a.Id == answerId) && (a.CodeSnippetIndex == answerCodeSnippetIndex));

                answer.ClusterId = clusterId;
                answer.Rank = answerRank;

                var foundResultGroup = queryResult.ResultGroups.FirstOrDefault(r => r.Id == clusterId);

                if (foundResultGroup == null)
                {
                    foundResultGroup = new ResultGroup(clusterId, clusterRank, answer);
                    queryResult.ResultGroups.Add(foundResultGroup);
                }
                else
                {
                    foundResultGroup.Answers.Add(answer);
                }
            }

            queryResult.SortResults();
            queryResult.CalculateCodeLines();

            return queryResult;
        }

        public static Task<QueryResult> QueryAsync(string queryText, string queryCode = null,
            string queryProgrammingLanguage = null, string queryTag = null)
        {
            return Task.Run(() => Query(queryText, queryCode, queryProgrammingLanguage, queryTag));
        }

        private static void RunAnalysis()
        {
            var processInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                UseShellExecute = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = $"/c \"conda activate SCoR && python {DirectoryManager.PythonAnalysisFile} --input {DirectoryManager.QueryResultJsonFile} --output {DirectoryManager.OutputAnalysisResultFile} && conda deactivate\""
            };

            var pythonProcess = Process.Start(processInfo);
            pythonProcess.WaitForExit();
            pythonProcess.Close();
        }
    }
}
