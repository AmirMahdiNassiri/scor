﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCoR.SDK.Data
{
    public class QABase : StackExchangeBase
    {
        #region Properties

        public const string TagsJsonName = "tags";
        [JsonProperty(TagsJsonName)]
        public List<string> Tags { get; set; }

        public const string DownVoteCountJsonName = "down_vote_count";
        [JsonProperty(DownVoteCountJsonName)]
        public int DownVoteCount { get; set; }

        public const string UpVoteCountJsonName = "up_vote_count";
        [JsonProperty(UpVoteCountJsonName)]
        public int UpVoteCount { get; set; }

        public const string LastActivityDateJsonName = "last_activity_date";
        [JsonProperty(LastActivityDateJsonName)]
        [JsonConverter(typeof(StackExchangeDateTimeConverter))]
        public DateTime LastActivityDate { get; set; }

        public const string LastEditDateJsonName = "last_edit_date";
        [JsonProperty(LastEditDateJsonName)]
        [JsonConverter(typeof(StackExchangeDateTimeConverter))]
        public DateTime LastEditDate { get; set; }

        public const string CommunityOwnedDateJsonName = "community_owned_date";
        [JsonProperty(CommunityOwnedDateJsonName)]
        [JsonConverter(typeof(StackExchangeDateTimeConverter))]
        public DateTime CommunityOwnedDate { get; set; }

        public const string LinkJsonName = "link";
        [JsonProperty(LinkJsonName)]
        public string Link { get; set; }

        public const string TitleJsonName = "title";
        [JsonProperty(TitleJsonName)]
        public string Title { get; set; }

        public const string CommentsJsonName = "comments";
        [JsonProperty(CommentsJsonName)]
        public List<Comment> Comments { get; set; }

        private int? _commentsUpVoteSum;
        public const string CommentsUpVoteSumJsonName = "comments_up_vote_sum";
        [JsonProperty(CommentsUpVoteSumJsonName)]
        public int CommentsUpVoteSum
        {
            get
            {
                if (!_commentsUpVoteSum.HasValue)
                    LoadCommentUpvoteSum();

                return _commentsUpVoteSum.Value;
            }
            set { _commentsUpVoteSum = value; }
        }

        #endregion

        #region Constructors

        public QABase()
        {
            Comments = new List<Comment>();
        }

        #endregion

        #region Methods

        private void LoadCommentUpvoteSum()
        {
            _commentsUpVoteSum = 0;

            foreach (var comment in Comments)
            {
                _commentsUpVoteSum += comment.Score;
            }
        }

        #endregion
    }
}
