﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCoR.SDK.Data
{
    /// <summary>
    /// See: https://api.stackexchange.com/docs/types/answer
    /// </summary>
    public class Answer : QABase
    {
        #region Properties

        public const string IdJsonName = "answer_id";
        [JsonProperty(IdJsonName)]
        public int Id { get; set; }

        public const string QuestionIdJsonName = "question_id";
        [JsonProperty(QuestionIdJsonName)]
        public int QuestionId { get; set; }

        public const string IsAcceptedJsonName = "is_accepted";
        [JsonProperty(IsAcceptedJsonName)]
        public bool IsAccepted { get; set; }

        public const string CodeSnippetIndexJsonName = "code_snippet_index";
        [JsonProperty(CodeSnippetIndexJsonName)]
        public int CodeSnippetIndex { get; set; } = 0;

        [JsonIgnore]
        public int ClusterId { get; set; }

        [JsonIgnore]
        public double Rank { get; set; }

        #endregion

        #region Constructors

        public Answer(int codeSnippetIndex = 0)
        {
            CodeSnippetIndex = codeSnippetIndex;
        }

        #endregion

        #region Methods

        /// <summary>
        /// This function divides this <see cref="Answer"/> instance into multiple Answers corresponding
        /// to each of the available <see cref="StackExchangeBase.CodeSnippets"/>. This instance would also
        /// be returned as the last item of the list.
        /// </summary>
        /// <returns></returns>
        public Tuple<List<Answer>, int> CreateSubAnswers(int codeLinesBefore, int codeDivisionThreshold = 20)
        {
            codeLinesBefore = CalculateCodeLines(codeLinesBefore);

            if (CodeSnippets.Count < 2)
                return new Tuple<List<Answer>, int>(new List<Answer> { this }, codeLinesBefore);

            var mergedInstance = new Answer();

            ReflectionHelper.CopyProperties(mergedInstance, this, nameof(CodeSnippets), nameof(CodeSnippetIndex));
            mergedInstance.CodeSnippets = new List<string> { string.Join(Environment.NewLine, CodeSnippets).Trim() };

            var result = new List<Answer>
            {
                mergedInstance
            };

            var dividedCodeSnippets = CodeSnippets.Where(c => codeDivisionThreshold < c.Length).ToList();

            for (int i = 0; i < dividedCodeSnippets.Count; i++)
            {
                // Create new answer
                var answer = new Answer(i + 1);

                // Copy properties from this instance
                ReflectionHelper.CopyProperties(answer, this, nameof(CodeSnippets), nameof(CodeSnippetIndex));

                // Put the single corresponding code snippet
                answer.CodeSnippets = new List<string> { dividedCodeSnippets[i].Trim() };

                result.Add(answer);
            }

            return new Tuple<List<Answer>, int>(result, codeLinesBefore);
        }

        #endregion
    }
}
