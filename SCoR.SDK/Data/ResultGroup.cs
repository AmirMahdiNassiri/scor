﻿using SCoR.SDK.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace SCoR.SDK.Data
{
    public class ResultGroup
    {
        #region Properties

        public int Id { get; set; }

        public double Rank { get; set; }

        public List<Answer> Answers { get; set; }

        #endregion

        #region Constructors

        public ResultGroup(int id, double rank, params Answer[] answers)
        {
            Id = id;
            Rank = rank;
            Answers = answers.ToList();
        }

        #endregion

        #region Public Methods

        public void SortAnswers()
        {
            Answers.Sort(new AnswerComparer());
        }

        #endregion
    }
}