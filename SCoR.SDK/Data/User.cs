﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.SDK.Data
{
    public class User
    {
        public const string IdJsonName = "user_id";
        [JsonProperty(IdJsonName)]
        public int Id { get; set; }

        public const string ReputationJsonName = "reputation";
        [JsonProperty(ReputationJsonName)]
        public int Reputation { get; set; }

        public const string TypeJsonName = "user_type";
        [JsonProperty(TypeJsonName)]
        public string Type { get; set; }

        public const string AcceptRateJsonName = "accept_rate";
        [JsonProperty(AcceptRateJsonName)]
        public int AcceptRate { get; set; }

        public const string ProfileImageJsonName = "profile_image";
        [JsonProperty(ProfileImageJsonName)]
        public string ProfileImage { get; set; }

        public const string DisplayNameJsonName = "display_name";
        [JsonProperty(DisplayNameJsonName)]
        public string DisplayName { get; set; }

        public const string LinkJsonName = "link";
        [JsonProperty(LinkJsonName)]
        public string Link { get; set; }
    }
}
