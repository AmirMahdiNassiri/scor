﻿using Newtonsoft.Json;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.SDK.Data
{
    /// <summary>
    /// See: https://api.stackexchange.com/docs/types/question
    /// </summary>
    public class Question : QABase
    {
        #region Properties

        public const string IdJsonName = "question_id";
        [JsonProperty(IdJsonName)]
        public int Id { get; set; }

        public const string IsAnsweredJsonName = "is_answered";
        [JsonProperty(IsAnsweredJsonName)]
        public bool IsAnswered { get; set; }

        public const string ViewCountJsonName = "view_count";
        [JsonProperty(ViewCountJsonName)]
        public int ViewCount { get; set; }

        public const string AnswerCountJsonName = "answer_count";
        [JsonProperty(AnswerCountJsonName)]
        public int AnswerCount { get; set; }

        public const string AcceptedAnswerIdJsonName = "accepted_answer_id";
        [JsonProperty(AcceptedAnswerIdJsonName)]
        public int AcceptedAnswerId { get; set; }

        public const string AnswersJsonName = "answers";
        [JsonProperty(AnswersJsonName)]
        public List<Answer> Answers { get; set; }

        #endregion

        #region Constructors

        public Question()
        {
            Answers = new List<Answer>();
        }

        #endregion

        #region Methods

        public int CreateSubAnswers(int codeLinesCountBefore)
        {
            var newAnswersList = new List<Answer>();

            foreach (var a in Answers)
            {
                a.Tags = Tags;

                var subAnswersResult = a.CreateSubAnswers(codeLinesCountBefore);
                
                newAnswersList.AddRange(subAnswersResult.Item1);
                codeLinesCountBefore = subAnswersResult.Item2;
            }

            Answers = newAnswersList;

            return codeLinesCountBefore;
        }

        #endregion
    }
}
