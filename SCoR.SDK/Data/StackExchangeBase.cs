﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCoR.SDK.Data
{
    public class StackExchangeBase
    {
        #region Properties

        public const string OwnerJsonName = "owner";
        [JsonProperty(OwnerJsonName)]
        public User Owner { get; set; }

        public const string OwnerReputationJsonName = "owner_" + User.ReputationJsonName;
        [JsonProperty(OwnerReputationJsonName)]
        public int OwnerReputation => Owner.Reputation;

        public const string OwnerAcceptRateJsonName = "owner_" + User.AcceptRateJsonName;
        [JsonProperty(OwnerAcceptRateJsonName)]
        public int OwnerAcceptRate => Owner.AcceptRate;

        public const string CreationDateJsonName = "creation_date";
        [JsonProperty(CreationDateJsonName)]
        [JsonConverter(typeof(StackExchangeDateTimeConverter))]
        public DateTime CreationDate { get; set; }

        public const string ScoreJsonName = "score";
        [JsonProperty(ScoreJsonName)]
        public int Score { get; set; }

        public const string ContentLicenseJsonName = "content_license";
        [JsonProperty(ContentLicenseJsonName)]
        public string ContentLicense { get; set; }

        public const string BodyJsonName = "body";
        [JsonProperty(BodyJsonName)]
        public string Body { get; set; }

        private string _bodyMessageOnly;
        public const string BodyMessageOnlyJsonName = "body_text_only";
        [JsonProperty(BodyMessageOnlyJsonName)]
        public string BodyMessageOnly
        {
            get
            {
                if (string.IsNullOrEmpty(_bodyMessageOnly))
                    ParseBody();

                return _bodyMessageOnly;
            }
            set { _bodyMessageOnly = value; }
        }

        private string _bodyAsText;
        [JsonIgnore]
        public string BodyAsText
        {
            get
            {
                if (string.IsNullOrEmpty(_bodyAsText))
                    ParseBody();

                return _bodyAsText;
            }
            set { _bodyAsText = value; }
        }

        private bool? _hasCodeSnippet;
        public const string HasCodeSnippetJsonName = "has_code_snippet";
        [JsonProperty(HasCodeSnippetJsonName)]
        public bool HasCodeSnippet
        {
            get
            {
                if (!_hasCodeSnippet.HasValue)
                    ParseBody();

                return _hasCodeSnippet.Value;
            }
            set
            {
                _hasCodeSnippet = value;
            }
        }

        private List<string> _codeSnippets;
        public const string CodeSnippetsJsonName = "code_snippets";
        [JsonProperty(CodeSnippetsJsonName)]
        public List<string> CodeSnippets
        {
            get
            {
                if (_codeSnippets == null)
                    ParseBody();

                return _codeSnippets;
            }
            set 
            {
                _codeSnippets = value; 
            }
        }

        [JsonIgnore]
        public int OriginalCodeLinesBefore { get; set; }

        [JsonIgnore]
        public int OriginalCodeLinesAfter { get; set; }

        [JsonIgnore]
        public int CodeLinesBefore { get; set; }

        [JsonIgnore]
        public int CodeLinesAfter { get; set; }

        #endregion

        #region Constructors

        public StackExchangeBase()
        {

        }

        #endregion

        #region Methods

        public int CalculateCodeLines(int codeLinesBefore, bool originalOrdering = true)
        {
            if (originalOrdering)
            {
                OriginalCodeLinesBefore = codeLinesBefore;
                OriginalCodeLinesAfter = codeLinesBefore;

                foreach (var c in CodeSnippets)
                {
                    OriginalCodeLinesAfter += (int)Math.Ceiling(c.Length / 70.0);
                }

                return OriginalCodeLinesAfter;
            }
            else
            {
                CodeLinesBefore = codeLinesBefore;
                CodeLinesAfter = codeLinesBefore;

                foreach (var c in CodeSnippets)
                {
                    CodeLinesAfter += (int)Math.Ceiling(c.Length / 70.0);
                }

                return CodeLinesAfter;
            }
        }

        private void ParseBody()
        {
            string GetText(HtmlNode node)
            {
                return HtmlEntity.DeEntitize(node.InnerText).Trim();
            }

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(Body);

            var textNodes = htmlDoc.DocumentNode.SelectNodes("//text()[normalize-space()]");

            _bodyAsText = string.Empty;
            _bodyMessageOnly = string.Empty;

            foreach (var node in textNodes)
            {
                if (node.ParentNode.Name == "code")
                {
                    _bodyAsText += Environment.NewLine + GetText(node) + Environment.NewLine;
                }
                else
                {
                    _bodyAsText += GetText(node) + " ";
                    _bodyMessageOnly += GetText(node) + " ";
                }
            }

            _bodyAsText.Trim();
            if (_bodyAsText.StartsWith(Environment.NewLine))
                _bodyAsText = _bodyAsText.Remove(0, Environment.NewLine.Length);

            var codeNodes = htmlDoc.DocumentNode.Descendants("code");

            if (codeNodes.Any())
                HasCodeSnippet = true;
            else
                HasCodeSnippet = false;

            CodeSnippets = new List<string>();

            foreach (var node in codeNodes)
            {
                CodeSnippets.Add(GetText(node));
            }
        }

        #endregion
    }
}
