﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SCoR.SDK.Data
{
    public class QueryResult
    {
        #region Properties

        public const string QueryTextJsonName = "query_text";
        [JsonProperty(QueryTextJsonName)]
        public string QueryText { get; set; }

        public const string QueryCodeJsonName = "query_code";
        [JsonProperty(QueryCodeJsonName)]
        public string QueryCode { get; set; }

        public const string QueryProgrammingLanguageJsonName = "query_programming_language";
        [JsonProperty(QueryProgrammingLanguageJsonName)]
        public string QueryProgrammingLanguage { get; set; }

        public const string QueryTagJsonName = "query_tag";
        [JsonProperty(QueryTagJsonName)]
        public string QueryTag { get; set; }

        public const string QuestionsJsonName = "result_questions";
        [JsonProperty(QuestionsJsonName)]
        public List<Question> ResultQuestions { get; set; }

        private List<Answer> _allAvailableAnswers;
        [JsonIgnore]
        public List<Answer> AllAvailableAnswers
        {
            get
            {
                if (_allAvailableAnswers == null)
                    LoadAllAvailableAnswers();

                return _allAvailableAnswers;
            }
            set { _allAvailableAnswers = value; }
        }

        [JsonIgnore]
        public List<ResultGroup> ResultGroups { get; }

        #endregion

        #region Constructors

        public QueryResult(string queryText, IList<Question> questionsWithAnswers,
            string queryCode = null, string queryProgrammingLanguage = null, string queryTag = null)
        {
            QueryText = queryText;
            QueryCode = queryCode;
            QueryProgrammingLanguage = queryProgrammingLanguage;
            QueryTag = queryTag;

            ResultQuestions = questionsWithAnswers.ToList();

            LoadAllAvailableAnswers();

            ResultGroups = new List<ResultGroup>();
        }

        #endregion

        #region Methods

        public void LoadAllAvailableAnswers()
        {
            _allAvailableAnswers = new List<Answer>();

            int codeLinesCounter = 0;

            foreach (var q in ResultQuestions)
            {
                codeLinesCounter = q.CreateSubAnswers(codeLinesCounter);
                _allAvailableAnswers.AddRange(q.Answers);
            }
        }

        public void SaveToJson(string filePath)
        {
            var jsonString = JsonConvert.SerializeObject(this, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ContractResolver = new ExportContractResolver()
                });
            
            using var writer = File.CreateText(filePath);

            writer.Write(jsonString);

            writer.Flush();
            writer.Close();
        }

        public void SortResults()
        {
            ResultGroups.Sort(new ResultGroupComparer());

            foreach (var g in ResultGroups)
            {
                g.SortAnswers();
            }
        }

        public void CalculateCodeLines(bool originalOrdering = false)
        {
            int codeLinesCounter = 0;

            foreach (var g in ResultGroups)
            {
                foreach (var a in g.Answers)
                {
                    codeLinesCounter = a.CalculateCodeLines(codeLinesCounter, originalOrdering);
                }
            }
        }

        #endregion
    }

    public class ExportContractResolver : DefaultContractResolver
    {
        public static List<string> StackExchangeBasePropertiesToSerialize = new List<string>
        {
            StackExchangeBase.BodyMessageOnlyJsonName,
            StackExchangeBase.HasCodeSnippetJsonName,
            StackExchangeBase.CodeSnippetsJsonName,
            StackExchangeBase.ScoreJsonName,
            StackExchangeBase.OwnerReputationJsonName,
            StackExchangeBase.OwnerAcceptRateJsonName
        };

        public static List<string> QABasePropertiesToSerialize = new List<string>(StackExchangeBasePropertiesToSerialize)
        {
            QABase.TitleJsonName,
            QABase.TagsJsonName,
            QABase.UpVoteCountJsonName,
            QABase.DownVoteCountJsonName,
            QABase.CommentsUpVoteSumJsonName,
            QABase.LastActivityDateJsonName
        };

        public static List<string> AnswerPropertiesToSerialize = new List<string>(QABasePropertiesToSerialize)
        {
            Answer.IdJsonName,
            Answer.QuestionIdJsonName,
            Answer.IsAcceptedJsonName,
            Answer.CodeSnippetIndexJsonName
        };

        public static List<string> QuestionPropertiesToSerialize = new List<string>(QABasePropertiesToSerialize)
        {
            Question.IdJsonName,
            Question.IsAnsweredJsonName,
            Question.ViewCountJsonName,
            Question.AnswerCountJsonName,
            Question.AcceptedAnswerIdJsonName,
            Question.AnswersJsonName
        };

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);

            if (type == typeof(Answer))
                properties = properties.Where(p => AnswerPropertiesToSerialize.Contains(p.PropertyName)).ToList();

            else if(type == typeof(Question))
                properties = properties.Where(p => QuestionPropertiesToSerialize.Contains(p.PropertyName)).ToList();

            return properties;
        }
    }
}
