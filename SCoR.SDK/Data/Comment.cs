﻿using Newtonsoft.Json;
using SCoR.SDK.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.SDK.Data
{
    /// <summary>
    /// See: https://api.stackexchange.com/docs/types/comment
    /// </summary>
    public class Comment : StackExchangeBase
    {
        #region Properties

        public const string IdJsonName = "comment_id";
        [JsonProperty(IdJsonName)]
        public int Id { get; set; }

        public const string PostIdJsonName = "post_id";
        [JsonProperty(PostIdJsonName)]
        public int PostId { get; set; }

        public const string EditedJsonName = "edited";
        [JsonProperty(EditedJsonName)]
        public bool Edited { get; set; }

        public const string ReplyToUserJsonName = "reply_to_user";
        [JsonProperty(ReplyToUserJsonName)]
        public User ReplyToUser { get; set; }

        #endregion
    }
}
