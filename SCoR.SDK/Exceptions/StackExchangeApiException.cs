﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SCoR.SDK.Exceptions
{
    public class StackExchangeApiException : Exception
    {
        public HttpStatusCode ErrorCode { get; private set; }

        public StackExchangeApiException(string message, HttpStatusCode errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
