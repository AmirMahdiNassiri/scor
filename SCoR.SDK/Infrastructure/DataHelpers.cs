﻿using SCoR.SDK.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.SDK.Infrastructure
{
    public class AnswerComparer : Comparer<Answer>
    {
        public override int Compare(Answer x, Answer y)
        {
            if (x.Rank > y.Rank)
                return -1;

            else if (x.Rank < y.Rank)
                return 1;

            else
                return 0;
        }
    }

    public class ResultGroupComparer : Comparer<ResultGroup>
    {
        public override int Compare(ResultGroup x, ResultGroup y)
        {
            if (x.Rank > y.Rank)
                return -1;

            else if (x.Rank < y.Rank)
                return 1;

            else
                return 0;
        }
    }
}
