﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace SCoR.SDK.Infrastructure
{
    public static class DirectoryManager
    {
        #region Properties

        private static string _companyName;
        public static string CompanyName
        {
            get
            {
                if (string.IsNullOrEmpty(_companyName))
                {
                    FillCompanyAndAppName();
                }

                return _companyName;
            }
        }

        private static string _appName;
        public static string AppName
        {
            get
            {
                if (string.IsNullOrEmpty(_appName))
                {
                    FillCompanyAndAppName();
                }

                return _appName;
            }
        }

        private static string _appDataFolder;
        public static string AppDataFolder
        {
            internal set
            {
                if (!Directory.Exists(value))
                    throw new DirectoryNotFoundException("Directory given to DataStreamerSdk.DirectoryManager.AppDataFolder does not exist. Ensure that the given directory exists.");

                var sdkAppDataFolder = Path.Combine(value, AppName);

                if (!Directory.Exists(sdkAppDataFolder))
                    Directory.CreateDirectory(sdkAppDataFolder);

                _appDataFolder = sdkAppDataFolder;
            }
            get
            {
                if (!string.IsNullOrWhiteSpace(_appDataFolder))
                    return _appDataFolder;

                var companyDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), CompanyName);

                if (!Directory.Exists(companyDirectory))
                {
                    Directory.CreateDirectory(companyDirectory);
                }

                var appDirectory = Path.Combine(companyDirectory, AppName);

                if (!Directory.Exists(appDirectory))
                {
                    Directory.CreateDirectory(appDirectory);
                }

                return appDirectory;
            }
        }

        public static string QueryResultJsonFile
        {
            get
            {
                return Path.Combine(AppDataFolder, "QueryResult.json");
            }
        }

        public static string OutputAnalysisResultFile
        {
            get
            {
                return Path.Combine(AppDataFolder, "Output.txt");
            }
        }

        public static string ExecutableDirectory
        {
            get
            {
                return new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
            }
        }

        public static string PythonAnalysisFile
        {
            get
            {
                return Path.Combine(ExecutableDirectory, "SCoR.Analysis.py");
            }
        }

        #endregion

        #region Methods

        private static void FillCompanyAndAppName()
        {
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetAssembly(typeof(SCoR)).Location);

            _companyName = fileVersionInfo.CompanyName;
            _appName = fileVersionInfo.ProductName;
        }

        #endregion
    }
}
