﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCoR.SDK.Infrastructure
{
    public class StackExchangeDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value is long apiDate)
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(apiDate);
            }

            throw new ArgumentException("Value is not an Integer.");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime dateTime)
            {
                writer.WriteValue(dateTime);

                // NOTE: The following lines are the method StackExchange uses to serialize DateTime

                //var unixEpochTime = (dateTime - new DateTime(1970, 1, 1)).TotalSeconds;

                //writer.WriteValue((int)unixEpochTime);
            }
            else
                throw new ArgumentException("Value is not a DateTime.");
        }
    }
}
