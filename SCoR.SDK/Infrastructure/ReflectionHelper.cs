﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCoR.SDK.Infrastructure
{
    public static class ReflectionHelper
    {
        public static int CopyProperties(object targetObject, object sourceObject, params string[] propertiesToExclude)
        {
            int copiedPropertiesCounter = 0;

            var targetType = targetObject.GetType();
            var sourceType = sourceObject.GetType();

            var sourcePropertiesToRead = sourceType.GetProperties().
                Where(p => !propertiesToExclude.Contains(p.Name) && p.CanRead);

            foreach (var sourceProperty in sourcePropertiesToRead)
            {
                var targetProperty = targetType.GetProperty(sourceProperty.Name);

                if (targetProperty != null && targetProperty.CanWrite)
                {
                    targetProperty.SetValue(targetObject, sourceProperty.GetValue(sourceObject));
                    copiedPropertiesCounter++;
                }
            }

            return copiedPropertiesCounter;
        }
    }
}
